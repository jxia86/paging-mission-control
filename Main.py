from datetime import datetime
import Alert_Queue as aq
import json

"""
Open data.txt file from the same directory and parse contents line by line.
If a line contains data that does cannot trigger alert (e.g does not exceed
either upper or lower bound), ignore it.  For the remaining lines, cast
from string to appropriate types and push into alert_queue.
"""
if __name__ == "__main__":
    SatList = {}
    with open('data.txt') as f:
        for data in f:
            data = data.strip('\n').split("|")
            
            if not ((data[7] == 'BATT' and float(data[6]) > float(data[5])) or (data[7] == 'TSTAT' and float(data[6]) < float(data[2]))):
                data[0] = datetime.strptime(data[0], "%Y%m%d %H:%M:%S.%f")
                data[7].strip()
                
                #if id is not a key in SatList, create the alert_queue.
                #if id is a key in SatList, push to alert_queue under that key.
                if data[1] not in SatList.keys():
                    SatList[data[1]] = aq.alert_queue(data[0], data[1], data[7])
                else:
                    SatList[data[1]].push(data[0], data[7])
    
    
    #Grab all alerts and print
    #JSON and ordereddict usage is strictly for formatting
    Alerts = []
    for _, value in SatList.items():
        Alerts = Alerts + value.RaisedAlerts
        
    print(json.dumps(Alerts, indent=4))

        
    
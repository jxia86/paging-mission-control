from datetime import datetime
import Alert_Queue as aq

def parsedata(data):
    data = data.strip('\n').split("|")
    
    if not ((data[7] == 'BATT' and float(data[6]) > float(data[5])) or (data[7] == 'TSTAT' and float(data[6]) < float(data[2]))):
        data[0] = datetime.strptime(data[0], "%Y%m%d %H:%M:%S.%f")
        data[7].strip()
        return data
    else:
        return None
        
if __name__ == "__main__":
    #test that data is fitered if it cannot cause alerts
    data = "20180101 01:00:00.000|1000|101|98|25|20|50|TSTAT"
    data = parsedata(data)
    assert data is None
    data = "20180101 01:00:00.000|1000|101|98|25|20|50|BATT"
    data = parsedata(data)
    assert data is None
    
    #test that alert_queue can be initialized and pushed onto
    data = "20180101 01:00:00.000|1000|101|98|25|20|102|TSTAT"
    data2 = "20180101 01:00:00.000|1000|101|98|25|20|10|BATT" 
    data = parsedata(data)
    data2 = parsedata(data2)
    alertqueue = aq.alert_queue(data[0], data[1], data[7])
    alertqueue.push(data2[0], data2[7])
    assert alertqueue.gettstatstatus() == [data[0]]
    assert alertqueue.getbattstatus() == [data2[0]]
    #test that id and time are stored correct
    assert alertqueue.Id == '1000'
    assert alertqueue.LowTime['TSTAT'] == data[0]
    assert alertqueue.LowTime['BATT'] == data2[0]
    
    #test that alert_queue pops data older than 5 minutes
    alertqueue.push(datetime(2018, 1, 1, 1, 2), 'BATT')
    alertqueue.push(datetime(2018, 1, 1, 1, 6), 'BATT')
    assert alertqueue.getbattstatus() == [datetime(2018, 1, 1, 1, 2), datetime(2018, 1, 1, 1, 6)]

    #test that raising an alert prints to console and clears queue (other queue unaffected)
    alertqueue.push(datetime(2018, 1, 1, 1, 7), 'BATT')
    assert alertqueue.getbattstatus() == []
    assert alertqueue.gettstatstatus() == [data[0]]
import datetime as datetime
from collections import OrderedDict
class alert_queue:
    """
    Creates a list for each component which stores timestamps and acts as 
    a queue.
    """
    def __init__(self, Time, Id, Component):
        """
        Parameters:
        -----------
        Time : datetime
            timestamp of values
        Id : str
            satellite-id
        Component : str
            component type ('BATT' or 'TSTAT')
        Description:
        ------------
        Take alert data and initialize an alert_queue.  Then call
        push() to insert the first data's timestamp into the 
        appropriate key of AlertTimes.
        """
        self.Id = Id
        self.LowTime = {'TSTAT':None, 'BATT':None}
        self.AlertTimes = {'TSTAT':[], 'BATT':[]}
        self.IsAlert = {'TSTAT':False, 'BATT':False}
        self.AlertSeverity = {'TSTAT':'RED HIGH', 'BATT':'RED LOW'}
        self.RaisedAlerts = []
        
        self.push(Time, Component)
    
    def push(self, Time, Component):    
        """
        Parameters:
        -----------
        Time : datetime
            timestamp of values
        Component : str
            component type ('BATT' or 'TSTAT')
        Description:
        ------------
        Takes a row of alert data and pushes it onto the applicable queue 
        (BATT or TSTAT).  Pops all items older than 5 minutes out of the
        queue.  If the queue size is 3 or more, call raise_alert() and clear
        queue.
        """
        if self.LowTime[Component] is None:
            self.LowTime[Component] = Time
        else:
            while Time - self.LowTime[Component] > datetime.timedelta(minutes = 5):
                self.AlertTimes[Component].pop(0)
                self.LowTime[Component] = self.AlertTimes[Component][0]
        
        self.AlertTimes[Component].append(Time)
        
        if len(self.AlertTimes[Component]) >= 3:
            self.raise_alert(Component)
            #resets the component queue after raising alert
            #(clear queue and LowTime)
            self.AlertTimes[Component] = []
            self.LowTime[Component] = None
        
    def raise_alert(self, Component):
        """
        Parameters:
        -----------
            Component : str
                component type ('BATT' or 'TSTAT')
        Description:
        ------------        
        Create alert structure and add it to raised alerts
        """
        alert = OrderedDict({
                    "satelliteId":int(self.Id), 
                    "severity": self.AlertSeverity[Component], 
                    "component":Component, 
                    "timestamp":self.LowTime[Component].strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'})
        self.RaisedAlerts.append(alert)
        
        
    """"Below functions only used for testing"""
    def gettstatstatus(self):
        return self.AlertTimes['TSTAT']
    
    def getbattstatus(self):
        return self.AlertTimes['BATT']
        
            
        
            
            
        
        